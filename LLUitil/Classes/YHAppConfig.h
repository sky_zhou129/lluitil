//
//  YHAppConfig.h
//  YueHuaYueYou
//
//  Created by puxiang on 2018/1/5.
//  Copyright © 2018年 中合农发. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YHAppConfig : NSObject

+ (NSString *)currentVersion;
+ (NSString *)buildVersion;
+ (NSString*)iphoneType;
+ (void)test;

@end
