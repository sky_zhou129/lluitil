#! /bin/bash


version="0.1.7"

echo
echo
echo
echo "-------------- 自制pod库开始 -------------- "

echo "1，版本号 version：$version"
# echo "更新内容 info：$info"


echo "2，校验本地库..."
# 忽略警告
pod lib lint --allow-warnings 

temp=${?}

go=0

if [[ temp -eq 0 ]]; then

	echo "3，打tag..."
	# 打tag
	git tag $version 


	echo "4，推送tag..."
	# 推送 tag
	git push --tag 


	echo "5，校验远程库..."
	# 校验远程库
	pod spec lint --allow-warnings


	temp=${?}

	if [[ temp -eq 0 ]]; then
		go=1;
	fi

else
	echo "本地校验失败！！！"
fi



if [[ go -eq 1 ]]; then

	echo "6，发布..."
	# 共有库
	pod trunk push --allow-warnings


	# echo "发布..."
	# 私有库
	# pod repo push --allow-warnings
	
else 
	echo "远程库 校验失败！！！！！！"
fi




