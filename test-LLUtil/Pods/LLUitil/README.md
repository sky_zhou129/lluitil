# LLUitil

[![CI Status](http://img.shields.io/travis/zhouwei/LLUitil.svg?style=flat)](https://travis-ci.org/zhouwei/LLUitil)
[![Version](https://img.shields.io/cocoapods/v/LLUitil.svg?style=flat)](http://cocoapods.org/pods/LLUitil)
[![License](https://img.shields.io/cocoapods/l/LLUitil.svg?style=flat)](http://cocoapods.org/pods/LLUitil)
[![Platform](https://img.shields.io/cocoapods/p/LLUitil.svg?style=flat)](http://cocoapods.org/pods/LLUitil)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

LLUitil is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'LLUitil'
```

## Author

zhouwei, zhouyikai129@163.com

## License

LLUitil is available under the MIT license. See the LICENSE file for more info.
