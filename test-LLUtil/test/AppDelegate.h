//
//  AppDelegate.h
//  test
//
//  Created by iOS-Dev05 on 2018/4/11.
//  Copyright © 2018年 CoolBit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

