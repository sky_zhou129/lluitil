#
# Be sure to run `pod lib lint LLUitil.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'LLUitil'
  s.version          = '0.1.7'
  s.summary          = 'A short description of LLUitil.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
  long description of the pod here.
                       DESC

  s.homepage         = 'https://sky_zhou129@bitbucket.org/sky_zhou129/LLUitil'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'zhouwei' => 'zhouyikai129@163.com' }
  s.source           = { :git => 'https://sky_zhou129@bitbucket.org/sky_zhou129/LLUitil.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'
  # https://sky_zhou129@bitbucket.org/sky_zhou129/lluitil.git

  s.ios.deployment_target = '8.0'

  # s.source_files = 'LLUitil/Classes/**/*'



  # s.source_files = 'LLUitil/Classes/**/*'
  # Classes文件夹下放的文件
  s.subspec 'Classes' do |cs|

    cs.source_files = 'LLUitil/Classes/**/*'

  end


  # Base文件夹下放的文件
  s.subspec 'Base' do |base|
    base.source_files = 'LLUitil/Base/**/*'
  end


  # Test文件夹下放的文件
  s.subspec 'Test' do |tes|
    tes.source_files = 'LLUitil/Test/*.{h,m,txt}'
  end


  
  # s.resource_bundles = {
  #   'LLUitil' => ['LLUitil/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
